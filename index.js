
const untildify = require('untildify')
const path = require('path')
const _ = require('lodash')

const filter = (ps) => _.map(ps, p => untildify(p))

const oldResolve = path.resolve
const oldNormalize = path.normalize

path.resolve = function TildeResolve(...paths)
{
  if (paths.length > 0)
    paths[0] = untildify(paths[0])
  return oldResolve(...paths)
}

path.normalize = function TildeNormalize(path) {
  return oldNormalize(untildify(path))
}

path.resolve.raw = oldResolve
path.normalize.raw = oldNormalize

// bc lodash doesn't feed resolve properly
path.resolve._ = (p) => path.resolve(p)

module.exports = path
