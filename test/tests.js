const path = require('..')

const assert = require('assert')
const os = require('os')

const _ = require('lodash')

const homedir = os.homedir()

describe('Tilde Path', function() {

  it('Knows `~` == `user.home`', () =>{
    assert.equal(homedir, path.resolve('~'))
    assert.equal(homedir, path.normalize('~'))
  })

  it('Resolves nested folders', function() {
    assert.equal( path.resolve('~/Documents') , `${homedir}/Documents` )
  })

  it('Resolves variadic concatenation', function() {
    assert.equal( path.resolve('~', 'Documents') , `${homedir}/Documents` )
  })

  it('Ignores not-leading tildes', function() {
    assert.equal( path.resolve('/usr/bin', '~') , `/usr/bin/~` )
  })

  path._resolve = (p) => path.resolve(p)

  it('Can be used with lodash ', function() {
    const values = ['~', '~/Documents', '~/Pictures']

    _.forEach( values, p => assert.notEqual( p, path.resolve(p) ) )

    _.map(values, path.resolve._)
    _.map(values, path.normalize)
  })

})
