# tilde-path
*Node Path with support for leading tilde home strings*

## Overview

### Problem

Developers and users shouldn't have to worry about where tilde strings are supported.

### Solution

This module patches the original `path` module so that leading tilde strings are handled by resolve and normalize.

## Installation

```
npm install @jamesarlow/tilde-path
```

## Usage

```
const path = require('tilde-path')

path.normalize('~')
// == path.resolve('~') 
// == os.homedir()

path.normalize('~/Pictures')
// == path.resolve('~/Pictures') 
// == os.homedir() + '/Pictures'

path.resolve('~', 'Documents')
// == path.normalize('~') + '/Documents'
// == os.homedir() + '/Documents'
```